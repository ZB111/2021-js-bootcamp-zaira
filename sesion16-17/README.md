Instalar dependencias y utilerías de webpack
npm i -D clean-webpack-plugin html-webpack-plugin webpack webpack-cli webpack-dev-server webpack-merge mini-css-extract-plugin css-minimizer-webpack-plugin

Instalar dependencias del entorno webpack
npm i -D css-loader node-sass sass-loader style-loader

Instalar dependencia Bootstrap 4.5 (última versión liberada)
npm i -D bootstrap

Instalar dependencia jQuery
npm i -D jquery

Instalar dependencia popper.js
npm i -D popper.js

Instalar dependencia fontawesome
npm i -D @fortawesome/fontawesome-free

Instalar dependencia chart.js
npm i -D chart.js

Conceptos de Bootstrap
Utilice las siguientes letras (clases) según corresponda:
m - margin
p - padding
d - display
Nota: Combine las clases m y p con las siguientes mostradas en la tabla según lo deseado.


Clase
Descripción

t
para clases que establecen margin-top o padding-top

b
para clases que establecen margin-bottom o padding-bottom

l
para clases que establecen margin-left o padding-left

r
para clases que establecen margin-right o padding-right

x
para clases que establecen tanto *-left y *-right

y
para clases que establecen tanto *-top y *-bottom



Para más detalles consulte la documentación.

Chart.js
En la siguiente liga podrás ver ejemplos de gráficos que puedes utilizar
https://www.chartjs.org/samples/master/
Nota: En las herramientas de desarrollo pestaña sources, revisar el código fuente del html, podrás ver el ejemplo de como crear la gráfica.
