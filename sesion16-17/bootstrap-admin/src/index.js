import './scss/styles.scss'
import './css/styles.css'
import 'jquery/dist/jquery.slim';
import 'popper.js'
import 'bootstrap/js/src/'
import '@fortawesome/fontawesome-free/js/all'
import 'chart.js'
import './js/line-chart'

(process.env.NODE_ENV !== 'production')
  ? () => console.log('Development mode 😎✌🏻')
  : () => console.log('Production mode 🚀')

console.log('*** Bootstrap Admin ***')

