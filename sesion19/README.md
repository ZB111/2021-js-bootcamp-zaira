Instalar dependencia browser-sync
Añadir en el archivo package.json, en la parte de scripts lo siguiente:
"scripts": {
    "start": "npx browser-sync src -w"
  }
NOTA: npx es una herramienta de cli que nos permite ejecutar paquetes de npm de forma sencilla sin necesidad de instalarlos, -w propiedad de browser-sync lo usamos para poder hacer reload automático de nuestro proyecto

Instalar Vue por CDN
Agregar el siguiente script en la etiqueta head del html:
<script src="https://unpkg.com/vue/dist/vue.js"></script>

Instalar Vue Router por CDN
Agregar el siguiente script en la etiqueta head del html:
<script src="https://unpkg.com/vue-router/dist/vue-router.js"></script>

Instalar Vuex por CDN
Agregar el siguiente script en la etiqueta head del html:
<script src="https://unpkg.com/vuex/dist/vuex.js"></script>

Instalar Vue Devtools
npm install -g @vue/devtools