Instalar dependencias y utilerías de webpack
npm i -D clean-webpack-plugin html-webpack-plugin webpack webpack-cli webpack-dev-server webpack-merge mini-css-extract-plugin css-minimizer-webpack-plugin

Instalar dependencias del entorno webpack
npm i -D css-loader node-sass sass-loader style-loader

Buscar comandos lanzados anteriormente en la consola
Tan solo debemos presionar ctrl + r y escribir un fragmento del comando a buscar, podemos ir saltando la busqueda presionando nuevamente ctrl + r

Borrar un directorio sin preguntar (forzado)
$ rm -rf node_modules