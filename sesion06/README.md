Instalar dependencias y utilerías de webpack
npm i -D webpack@4.44.2
npm i -D webpack-cli@3.3.12
npm i -D webpack-dev-server
npm i -D html-webpack-plugin
npm i -D clean-webpack-plugin

Instalar dependencias del entorno webpack
npm i -D css-loader node-sass sass-loader style-loader

Ejecutar
npm run start:dev