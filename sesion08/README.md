Copiar archivos de webpack de la sesión anterior desde consola
cp ../../sesion07/css-grid-demo/webpack.*.js .

Instalar dependencias y utilerías de webpack
npm i -D clean-webpack-plugin html-webpack-plugin webpack webpack-cli webpack-dev-server webpack-merge mini-css-extract-plugin css-minimizer-webpack-plugin

Instalar dependencias del entorno webpack
npm i -D css-loader node-sass sass-loader style-loader

Editar el archivo package.json con vim desde consola
vim package.json

Copiar archivos index.html y index.js de la sesión anterior
cp ../../sesion07/css-grid-demo/src/*./src

Ejemplo Emmet Abbreviation del proyecto
div.wrapper>(header{Header}+aside*2{Aside $}+section*2{Section $}+footer{Footer})

Buscar nodos con la consola de navegador
> document.querySelector('aside:nth-of-type(1)')
> document.querySelector('aside:nth-child(2)')