Instalar dependencias y utilerías de webpack
npm i -D clean-webpack-plugin html-webpack-plugin webpack webpack-cli webpack-dev-server webpack-merge mini-css-extract-plugin css-minimizer-webpack-plugin

Instalar dependencias del entorno webpack
npm i -D css-loader node-sass sass-loader style-loader

Instalar dependencia Bulma
npm i -D bulma

Instalar dependencia fontawesome
npm i -D @fortawesome/fontawesome-free