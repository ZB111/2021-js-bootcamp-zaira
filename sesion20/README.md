Instalar Vue-cli por npm
npm install -g @vue/cli

Crear proyecto Vue por terminal
Con el comando vue create seguido del nombre del proyecto:
vue create my-first-vue-app

Crear proyecto Vue por interfaz gráfica
Con el comando vue ui ejecutamos la interfaz gráfica:
vue ui