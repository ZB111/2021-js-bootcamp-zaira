Intsalar dependencia Jest
Para instalar jest en modo desarrollo ejecutamos en la terminal el siguiente comando:
npm i -D jest

Intsalar dependencia babel-jest y @babel/preset-env
npm i -D babel-jest @babel/preset-env

NOTA: En el archivo package.json añadimos en la parte de scripts lo siguiente:
"jest": {
    "transform": {
      "^.+\\.[t|j]sx?$": "babel-jest"
    }
  }

Posterior añadiremos en la raiz del proyecto un archivo babel.config.json para tener completo nuestro ambiente de pruebas unitarias, en el archivo añadir lo siguiente:
{
  "presets": [
    "@babel/preset-env"
  ]
}