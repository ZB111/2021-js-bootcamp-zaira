Instalar dependencia browser-sync
Añadir en el archivo package.json, en la parte de scripts lo siguiente:

"scripts": {
    "start": "npx browser-sync src -w"
  }


NOTA: npx es una herramienta de cli que nos permite ejecutar paquetes de npm de forma sencilla sin necesidad de instalarlos, -w propiedad de browser-sync lo usamos para poder hacer reload automático de nuestro proyecto
