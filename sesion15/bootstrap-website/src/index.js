import './scss/styles.scss'
import './css/styles.css'
import 'bootstrap/js/src/carousel'
import '@fortawesome/fontawesome-free/js/all'

(process.env.NODE_ENV !== 'production')
  ? () => console.log('Development mode 😎✌🏻')
  : () => console.log('Production mode 🚀')

console.log('*** Bootstrap Website ***')
